import PropTypes from "prop-types";
import React, {useState} from "react";
import { useTranslation } from 'react-i18next';
import { ButtonLink } from './../components/button-group/button-group';

const NavigationItem = props => (
  <li className="dropdown">
    <a href={props.href} className="scroll-link">
      <span className="nav-item-custom">
        {props.text}
      </span>
    </a>
  </li>
);

const Header = props => {
  const [isOpen, setIsOpen] = useState(false);
  const { t, i18n } = useTranslation();

  const navigationLinks = props.navLinks && Array.isArray(props.navLinks)
    ? props.navLinks.map((navLink, i) => (
        <NavigationItem
          key={i}
          text={t(navLink.label)}
          href={navLink.href}
          external={!!navLink.external}
        />
      ))
    : [];

  const languageLabel = i18n.language === "en" ? "el" : "en";

  return (
    <div className="nav-container" id="start">
      <div className="bar bar--sm visible-xs">
        <div className="container">
          <div className="row">
            <div className="col-12 col-md-12 text-right">
              <button
                className="hamburger-toggle border-0"
                onClick={ () => {setIsOpen(!isOpen)} }
              >
                <i className="icon icon--sm stack-interface stack-menu"/>
              </button>
            </div>
          </div>
        </div>
      </div>

      <nav id="menu1" className={isOpen ? "bar bar--sm bar-1" : "bar bar--sm bar-1 hidden-xs"}>
        <div className="container">
          <div className="row">
            <div className="col-lg-11 col-md-12 text-right text-left-xs text-left-sm">
              <div className="bar__module">
                <ul className="menu-horizontal text-left">
                  {navigationLinks}
                  <li className="dropdown">
                    <button className="scroll-link border-0 heigh-auto cursor-pointer"
                      onClick={() => {props.changeLang()}}
                    >
                      <span className="nav-item-custom">
                        { languageLabel }
                      </span>
                    </button>
                  </li>
                </ul>
              </div>

              <div className="bar__module">
                <ButtonLink
                  text={'Generic.Contact'}
                  href={`mailto:${t('Sections.Contact.EmailSubject')}`}
                />
              </div>
            </div>
          </div>
        </div>
      </nav>
    </div>
  )
};

Header.propTypes = {
  siteTitle: PropTypes.string
};

Header.defaultProps = {
  siteTitle: ``
};

export default Header;
