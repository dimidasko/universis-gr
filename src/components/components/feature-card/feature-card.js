import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { images } from './../../../images/images';

/**
 *
 * FeatureCardWithIcon component
 * Is the card with the left alignment and the icon
 *
 */
export const FeatureCardWithIcon = props => {
  const { t } = useTranslation();

  const splitedTitle = props.title.split(' ');
  const fullTitle = splitedTitle.map((chunk) => t(chunk)).join(' ');

  const extraClass = props.inline
    ? ['min-height-unset'].join(' ')
    : '';

  if( !props.containsURL){
      return (
          <div className={`feature feature-4 boxed boxed--lg boxed--border ${extraClass}`}>
              <i className={`icon ${props.icon}`} />
              <h4 className={props.inline ? 'd-inline ml-4' :''}>{fullTitle}</h4>
              <hr />
              <p>{t(props.body)}</p>
          </div>
      )
  } else {
      return (
          <div className={`feature feature-4 boxed boxed--lg boxed--border ${extraClass}`}>
              <i className={`icon ${props.icon}`}/>
              <h4 className={props.inline ? 'd-inline ml-4' : ''}>{fullTitle}</h4>
              <hr/>
              <p dangerouslySetInnerHTML={{__html: t(props.body)}}></p>
          </div>
      )
  }
};

/**
 *
 * FeatureCardWithIcon component
 * Is the card with the center alignment and the image at the top
 *
 */
export const FeatureCardWithImage = props => {
  const { t } = useTranslation();

  return (
    <div className="feature feature-3 boxed boxed--lg boxed--border text-center">
      <img className="icon-three-2" src={images[props.imageName]} alt={t(props.imageAlt)} />
      <h4 className="type--bold">{t(props.title)}</h4>
      <br />
      <p className="lead">{t(props.body)}</p>
    </div>
  )
};

/**
 *
 * JustImageFeatureCard component
 * Is the card which shows only the image
 *
 */
export const JustImageFeatureCard = props => {
  const { t } = useTranslation();

  return (
    <div className="feature feature-1">
      <img
        alt={t(props.imageAlt)}
        src={images[props.imageName]}
      />
      <div className="feature__body boxed boxed--border">
        <h5 className="demo-box">
          {t(props.title)}
        </h5>
      </div>
    </div>
  )
};

/**
 *
 * FeatureCard component
 *
 * It's the card with the grey border
 *
 */
export const FeatureCard = props => {

  let child;

  if (props.featureType === 'justImage') {
   child = <JustImageFeatureCard {...props} />;
  } else if (props.featureType === 'imageFirst') {
    child = <FeatureCardWithImage {...props} />;
  } else if (props.featureType === 'withIcon' || props.featureType === 'withIconInline') {
    child = <FeatureCardWithIcon {...props} inline />;
  }

  const cols = props.featureType === 'withIconInline'
    ? 6
    : 4;

  return (
    <div className={`col-md-${cols}`}>
      {child}
    </div>
  );
};

FeatureCard.propTypes = {
  title: PropTypes.string,
  body: PropTypes.string,
  image: PropTypes.string,
  imageAlt: PropTypes.string,
  featureType: PropTypes.oneOf(['justImage', 'imageFirst', 'withIcon', 'withIconInline']),
  containsURL: PropTypes.bool,
}

export default FeatureCard;
