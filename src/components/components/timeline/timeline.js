import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

/**
 *
 * TimelineItem component
 * It is a single entry at the timeline,
 * it consists of a title and some text
 *
 */
const TimelineItem = props => {
  const { t } = useTranslation();
  const [month, year] = props.title.split(' ');

  return (
    <div className={`col-md-4`}>
      <div className="process__item">
        <h5>{`${t(month)} ${year}`}</h5>
        <p>{t(props.body)}</p>
      </div>
    </div>
  );
}

/**
 *
 * TimelineSection component
 * It is a single row of timeline items.
 *
 */
export const TimelineSection = props => {
  const timelineItems = props.items.map((item, i) => (
    <TimelineItem key={i} {...item} />
  ));
  return (
    <div className="timeline-section mb-4 w-100">
      <div className="container mb-4">
        <div className="process-2 row">
          {timelineItems}
        </div>
      </div>
    </div>
  )
};

/**
 *
 * Timeline component
 * It wraps the whole list of the timeline items divided in sections
 *
 */
export const Timeline = props => {
  const timelineSections = props.sections.map((section, i) => (
    <TimelineSection key={i} items={section.items} />
  ));

  return (
    <>
      {timelineSections}
    </>
  )
};

Timeline.propTypes = {
  sections: PropTypes.array
}

TimelineSection.propTypes = {
  items: PropTypes.array
}

TimelineItem.protoTypes = {
  title: PropTypes.string,
  body: PropTypes.string,
}

export default Timeline;
