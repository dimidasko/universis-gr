import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

/**
 * 
 * ButtonLink component a link that looks like button with the default style
 * 
 */
export const ButtonLink = props => {
  
  const { t } = useTranslation();
  
  return (
    <a 
      className="btn btn--primary type--uppercase" 
      href={props.href} 
      target={props.external ? '_blank' : undefined}  
      rel={props.external ? 'noopener noreferrer nofollow' : undefined}  
    >
      <span className="btn__text">
        {t(props.text)}
      </span>
    </a>  
    );
};

/**
 * 
 * ButtonGroup component
 * A list of buttons with an optional title
 * 
 */
export const ButtonGroup = props => {
  
  const { t } = useTranslation();
  
  const buttons = props.buttons.map((button, i) => (
    <ButtonLink key={i} {...button} />
  ));
  
  return (
    <div className="row text-center">
      <div className="col">
        <p className="lead">
          <strong>{t(props.title)}</strong>
        </p>
        {buttons}
      </div>
    </div>
  );
};

/**
 * 
 * @interface ButtonLinkProps
 * 
 * @property {string} text The text to be shown in the button
 * @property {string} href The value to be used as the link's href
 * @property {boolean} external A flag to show if it is external link
 * 
 */
ButtonLink.propTypes = {
  text: PropTypes.string.isRequired,
  href: PropTypes.string.isRequired,
  external: PropTypes.bool
}

/**
 * 
 * @interface ButtonGroupProps
 * 
 * @property {string} title An optional title for the button group
 * @property {Array<ButtonLinkProps>} buttons An array of button data
 * 
 */
ButtonGroup.propTypes = {
  title: PropTypes.string,
  buttons: PropTypes.array
}

export default ButtonGroup;
