import i18n from "i18next"
import Backend from "i18next-xhr-backend"
import { initReactI18next } from 'react-i18next';

// Is available only if there is a browser
if (typeof window !== `undefined`) {
  i18n
    .use(Backend)
    .use(initReactI18next)
    .init({
      fallbackLng: "el",
      ns: ["translation"], // have a common namespace used around the full app
      defaultNS: "translation",    
      debug: process.env.NODE_ENV === 'dev' || process.env.NODE_ENV === 'development',
      interpolation: {
        escapeValue: false, // not needed for react!!
      },
      react: {
        wait: true,
        useSuspense: false
      },
    });
}  

export default i18n;
