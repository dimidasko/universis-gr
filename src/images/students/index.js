import studentsApp1 from './1-dashboard.jpg';
import studentsApp2 from './2-registration-semester.jpg';
import studentsApp3 from './3-registration-semester-2.jpg';
import studentsApp4 from './4-course-overview.jpg';
import studentsApp5 from './5-course-overview.jpg';
import studentsApp6 from './6-course-overview.jpg';
import studentsApp7 from './7-registrations-course-overview.jpg';
import studentsApp8 from './8-registrations-course-overview-2.jpg';
import studentsApp9 from './9-registrations-course-checkout.jpg';
import studentsApp10 from './10-registrations-course-checkout-2.jpg';
import studentsApp11 from './11-registrations-course-checkout-3.jpg';
import studentsApp12 from './12-registrations-list.jpg';
import studentsApp13 from './13-grades-recent.jpg';
import studentsApp14 from './14-statistcs.jpg';
import studentsApp15 from './15-grades-all.jpg';
import studentsApp16 from './16-grades-request.jpg';
import studentsApp17 from './17-examParticipation.jpg';
import studentsApp18 from './18-theses.jpg';
import studentsApp19 from './19-requests-list.jpg';
import studentsApp20 from './20-request-new.jpg';
import studentsApp21 from './21-request-new-2.jpg';
import studentsApp22 from './22-messages.jpg';
import studentsApp23 from './23-department.jpg';
import studentsApp24 from './24-profile.jpg';

export default {
  studentsApp1,
  studentsApp2,
  studentsApp3,
  studentsApp4,
  studentsApp5,
  studentsApp6,
  studentsApp7,
  studentsApp8,
  studentsApp9,
  studentsApp10,
  studentsApp11,
  studentsApp12,
  studentsApp13,
  studentsApp14,
  studentsApp15,
  studentsApp16,
  studentsApp17,
  studentsApp18,
  studentsApp19,
  studentsApp20,
  studentsApp21,
  studentsApp22,
  studentsApp23,
  studentsApp24
}
