// This file contains the images until they' re correctly sourced using gatsby-node
// this mapping enables the decoupling of the data with the source code

import authLogo from './../../public/img/team/auth.png'
import duthLogo from './../../public/img/team/duth.png'
import versionControl from './../assets/images/undraw_version_control_9bpv.svg';
import upgrade from './../assets/images/undraw_upgrade_06a0.svg';
import scrum from './../assets/images/undraw_scrum_board_cesn.svg'
import studentsApp from './studentsApp.png';
import facultyApp from './facultyApp.png';
import registrarApp from './registrarApp.png';
import facultyAppImages from './faculty';
import studentsAppImages from './students';
import registrarImages from './registrar';

export const images = {
  authLogo,
  duthLogo,
  versionControl,
  upgrade,
  scrum,
  studentsApp,
  facultyApp,
  registrarApp,
  ...facultyAppImages,
  ...studentsAppImages,
  ...registrarImages
}

export default images;
