import React from "react";
import { graphql } from 'gatsby';
import { withTranslation } from 'react-i18next';
import Header from "../components/containers/header";
import Hero from "../components/containers/hero/hero";
import PageSection from '../components/containers/page-section/page-section';
import Contact from './../components/containers/contact/contact';
import Footer from './../components/components/footer/footer';
import i18n from './../locales/i18n';

class IndexPage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.data = this.props.data.allContentJson.edges[0].node;
  }

  changeLanguage = () => {
    const language = i18n.language === "en" ? "el" : "en";
    i18n.changeLanguage(language);
  };

  /**
   *
   * Generates the sections content based on the app configuration
   *
   */
  getSections = (sectionData) => {
    if  (!sectionData) {
      return [];
    }

    const sections = sectionData.map((section, i) => {
      const isSecondary = i % 2 === 0;
      return (
        <PageSection
          key={i}
          title={section.title}
          name={section.name}
          subtitle={section.subtitle}
          isSecondary={isSecondary}
          childrenType={section.childrenType}
          childrenData={section.children}
          buttonGroup={section.buttonGroup}
        />
      );
    });

    return sections;
  }

  getNavigationData = (sectionData) => {
    if  (!sectionData) {
      return [];
    }

    const navigationData = sectionData
    .filter((section) => section.name && section.navigationLabel)
    .map((section) => ({
      label: section.navigationLabel,
      href: `#${section.name}`
    }));

    return navigationData;
  }

  render() {
    const snapshot = {...this.state}
    const { language } = snapshot;
    const journal = this.data.sections.filter((section) => section.name === 'journal');
    const mainSections = this.data.sections.filter((section) => section.name !== 'journal');
    const sections = this.getSections(mainSections.filter(section => section.name !== 'News'));
    const journalSection = this.getSections(journal);
    const navigationData = this.getNavigationData(this.data.sections);


    return (
      <>
      <Header
          navLinks={navigationData}
          changeLang={() => {this.changeLanguage()}}
        />
        <div className="main-container">
          <Hero lang={language}  {...this.data.hero} />
          {sections}
          <Contact info={this.data.contactInformation} />
          {journalSection}
          <Footer
            leftText={this.data.footer.footerSlogan}
            copyright={this.data.footer.copyright}
          />
        </div>
      </>
    );
  }
}

export default withTranslation()(IndexPage);

export const query = graphql`
  query {
    allContentJson {
      edges {
        node {
          contactInformation {
            contactEmail
            contactEmailSubject
            title
            subtitle
            buttonGroupTitle
          },
          footer {
            footerSlogan
            copyright
          },
          hero {
            ...HeroData
          },
          sections {
            ...SectionsData
          }
        }
      }
    }
  }
`;
