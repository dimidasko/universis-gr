import React from "react";
import Header from "../components/containers/header";
import Hero from "../components/containers/hero/hero";
import Footer from "../components/components/footer/footer";
import {withTranslation} from "react-i18next";
import PageSection from "../components/containers/page-section/page-section";
import {graphql} from "gatsby";
import i18n from "../locales/i18n";


class NewsPage extends React.PureComponent {
    constructor(props) {
        super(props);
        this.data = this.props.data.allContentJson.edges[0].node;
    }

    changeLanguage = async() => {
        const language = i18n.language === "en" ? "el" : "en";
        await i18n.changeLanguage(language);
    };

    getSections = (sectionData) => {
        if (!sectionData) {
            return [];
        }

        const sections = sectionData.map((section, i) => {
            const isSecondary = i % 2 === 0;
            return (
                <PageSection
                    key={i}
                    title={section.title}
                    name={section.name}
                    subtitle={section.subtitle}
                    isSecondary={isSecondary}
                    childrenType={section.childrenType}
                    childrenData={section.children}
                    buttonGroup={section.buttonGroup}
                />
            );
        });

        return sections;
    }

    getNavigationData = (sectionData) => {
        if (!sectionData) {
            return [];
        }

        const navigationData = sectionData
            .filter((section) => section.name && section.navigationLabel)
            .map((section) => {
                return section.navigationLabel !== 'Sections.News.News' ? {
                        label: section.navigationLabel,
                        href: `/#${section.name}`
                    } :
                    {
                        label: section.navigationLabel,
                        href: `#${section.name}`
                    }
            });

        return navigationData;
    }

    render() {
        const snapshot = {...this.state}
        const {language} = snapshot;
        const newsSection = this.data.sections.filter((section) => section.name === 'News');
        const sections = this.getSections(newsSection);
        const navigationData = this.getNavigationData(this.data.sections.filter((section)=> section.label !== 'journal'));
        this.data.hero.button = {...this.data.hero.button, href: `/${this.data.hero.button.href}`};


        return (
            <>
                <Header
                    navLinks={navigationData}
                    changeLang={ async() => {
                        await this.changeLanguage()
                    }}
                />
                <div className="main-container">
                    <Hero lang={language}  {...this.data.hero} />
                    {sections}
                    <Footer
                        leftText={this.data.footer.footerSlogan}
                        copyright={this.data.footer.copyright}
                    />
                </div>
            </>
        );
    }
}

export default withTranslation()(NewsPage);

export const query = graphql`
    query {
        allContentJson {
            edges {
                node {
                    contactInformation {
                        contactEmail
                        contactEmailSubject
                        title
                        subtitle
                        buttonGroupTitle
                    },
                    footer {
                        footerSlogan
                        copyright
                    },
                    hero {
                        ...HeroData
                    },
                    sections {
                        ...SectionsData
                    }
                }
            }
        }
    }
`;


