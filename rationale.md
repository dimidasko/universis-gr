# Rationale

## Facts

The design consists of:

- a hero
- x sections
- footer

Each of the sections (and hero) has:

- A title
- A subtitle
- text or list of cards
- A CTA title
- A CTA

Components:

- Card (the white box)
- Teaser
  - Center aligned teaser  (image, title, text vertically)
  - Left aligned teaser (small image, big title, text)
  - Image preview teaser
- Timeline items (title text)
- Titled image

## Decisions

A single json file that will describe each section for the sections with the
common layout (title, subtitle, list of other components)

The others will have their own json file

## Issues

The translation could be an issue, but we' ll see